#pragma once

#include <Arduino.h>
#include <RF24.h>

#include "DataPacket.hpp"

class UniversalRobotClient {
public:
    UniversalRobotClient(uint16_t ce, uint16_t csn, uint64_t address = 0xE7E7E7E6E6E6, uint8_t pipe = 1);
    void init();
    bool read(DataPacket& packet);
    void zero(const DataPacket& packet);

    void setDeadzone(int deadzone) { this->deadzone = deadzone; }
    int getDeadzone() const { return deadzone; }

private:
    RF24* radio;
    const uint8_t pipe;
    const uint64_t address;
    int deadzone = 0;
    DataPacket zeroPacket;

    void adjustJoystick(int8_t& raw, int8_t center) const;
};
