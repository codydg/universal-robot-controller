#pragma once

#include <cstdint>

#define DP_MAX_JOY_VAL 127

#define DP_DATA_TYPE int32_t
#define DP_BASE_VALUE ((DP_DATA_TYPE) 1 )
// Lettered buttons
#define DP_A DP_BASE_VALUE
#define DP_B (DP_A << 1)
#define DP_X (DP_A << 2)
#define DP_Y (DP_A << 3)
// Reserved bits
#define RESERVED_1 (DP_BASE_VALUE << 4)
#define RESERVED_2 (RESERVED_1 << 1)
#define RESERVED_3 (RESERVED_1 << 2)
#define RESERVED_4 (RESERVED_1 << 3)
// DPAD
#define DP_DPAD_UP    (DP_BASE_VALUE << 8)
#define DP_DPAD_DOWN  (DP_DPAD_UP << 1)
#define DP_DPAD_LEFT  (DP_DPAD_UP << 2)
#define DP_DPAD_RIGHT (DP_DPAD_UP << 3)
// Bumpers and Triggers
#define DP_LEFT_BUMPER   (DP_BASE_VALUE << 12)
#define DP_RIGHT_BUMPER  (DP_LEFT_BUMPER << 1)
#define DP_LEFT_TRIGGER  (DP_LEFT_BUMPER << 2)
#define DP_RIGHT_TRIGGER (DP_LEFT_BUMPER << 3)
// Back Buttons
#define DP_LEFT_BACK  (DP_BASE_VALUE << 16)
#define DP_RIGHT_BACK (DP_LEFT_BACK << 1)
// Joystick Buttons
#define DP_LEFT_JOY  (DP_BASE_VALUE << 18)
#define DP_RIGHT_JOY (DP_LEFT_JOY << 3)
// Misc Toggles
#define DP_TOGGLE_1 (DP_BASE_VALUE << 20)
#define DP_TOGGLE_2 (DP_TOGGLE_1 << 1)
#define DP_TOGGLE_3 (DP_TOGGLE_1 << 2)
#define DP_TOGGLE_4 (DP_TOGGLE_1 << 3)
// Home/Select/Start
#define DP_SELECT (DP_BASE_VALUE << 24)
#define DP_HOME   (DP_SELECT << 1)
#define DP_START  (DP_SELECT << 2)
// Reserved Bits
#define RESERVED_5 (DP_BASE_VALUE << 27)
#define RESERVED_6 (RESERVED_5 << 1)
#define RESERVED_7 (RESERVED_5 << 2)
#define RESERVED_8 (RESERVED_5 << 3)
#define RESERVED_9 (RESERVED_5 << 4)

struct DataPacket {
    int8_t left_x = 0;
    int8_t left_y = 0;

    int8_t right_x = 0;
    int8_t right_y = 0;

    DP_DATA_TYPE bits = 0;

    inline bool getBtn(DP_DATA_TYPE mask) const {
        return bits & mask;
    }
    inline void setBtn(bool value, DP_DATA_TYPE mask) {
        if (value) {
            bits |= mask;
        } else {
            bits &= ~mask;
        }
    }
};
