#pragma once

#include <Arduino.h>
#include <RF24.h>

#include "DataPacket.hpp"

class UniversalRobotServer {
public:
    UniversalRobotServer(uint16_t ce = 0, uint16_t csn = 1, uint64_t address = 0xE7E7E7E6E6E6);
    void init();
    bool write(const DataPacket& packet);

private:
    RF24* radio;
    const uint64_t address;
};
