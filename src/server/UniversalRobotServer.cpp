#include "UniversalRobotServer.hpp"

UniversalRobotServer::UniversalRobotServer(uint16_t ce, uint16_t csn, uint64_t address)
    : address(address) {
    radio = new RF24(ce, csn);
}

void UniversalRobotServer::init() {
    radio->begin();
    radio->stopListening();
    radio->openWritingPipe(address);
}

bool UniversalRobotServer::write(const DataPacket& packet) {
    return radio->write(&packet, sizeof(packet));
}
