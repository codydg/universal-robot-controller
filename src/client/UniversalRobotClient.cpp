#include "UniversalRobotClient.hpp"

UniversalRobotClient::UniversalRobotClient(uint16_t ce, uint16_t csn, uint64_t address, uint8_t pipe)
    : pipe(pipe), address(address) {
    radio = new RF24(ce, csn);
}

void UniversalRobotClient::init() {
    radio->begin();
    radio->openReadingPipe(pipe, address);
    radio->startListening();
}

bool UniversalRobotClient::read(DataPacket& packet) {
    if (!radio->available()) {
        return false;
    }

    radio->read(&packet, sizeof(packet));

    adjustJoystick(packet.left_x, zeroPacket.left_x);
    adjustJoystick(packet.left_y, zeroPacket.left_y);
    adjustJoystick(packet.right_x, zeroPacket.right_x);
    adjustJoystick(packet.right_y, zeroPacket.right_y);

    return true;
}

void UniversalRobotClient::zero(const DataPacket& packet) {
    zeroPacket = packet;
}

void UniversalRobotClient::adjustJoystick(int8_t& raw, int8_t center) const {
    // Fix Centering
    if (raw > center) {
        raw = map(raw, center, DP_MAX_JOY_VAL, 0, DP_MAX_JOY_VAL);
    } else {
        raw = map(raw, -DP_MAX_JOY_VAL, center, -DP_MAX_JOY_VAL, 0);
    }

    // Apply mapped deadzone
    if (raw > deadzone) {
        raw = map(raw, deadzone, DP_MAX_JOY_VAL, 0, DP_MAX_JOY_VAL);
    } else if (raw < -deadzone) {
        raw = map(raw, -DP_MAX_JOY_VAL, -deadzone, -DP_MAX_JOY_VAL, 0);
    } else {
        raw = 0;
    }
}
